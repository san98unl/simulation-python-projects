#Importacion de Librerias
import numpy as np
import h5py
import sys

#import matplotlib.pyplot as plt
#import statistics as stats

#Variables de Entrada

# Comprobación de seguridad, ejecutar sólo si se reciben 2 argumentos reales
if len(sys.argv) == 5: # Se comprueba que de 4 argumentos
    x0 = int(sys.argv[1])
    y0 = int(sys.argv[2])
    xn = int(sys.argv[3])
    n = int(sys.argv[4])+1
    result=np.zeros((n, 3)) # Matriz vacía, con valores residuales de la memoria
    #llenar columna n
    for i in range(n):
        result[i,0]=i

    #se calcula h
    h=round((xn-x0)/n,3)
    #h=(xn-x0)/n

    #llenar columna x0
    for i in range(n):
        if i==0:
            result[i,1]=x0
        else:
            result[i,1]=result[i-1,1]+h
    #
    for i in range(n):
        if i==0:
            result[i,2]=y0
        else:
            result[i,2]=result[i-1,2]+(h*((3*result[i-1,1])-(2*result[i-1,2])))

    #print(result) 
    #print("final",result[1000,2])  
    #print("h",h)   
    #hp5y
    print(result)
    with h5py.File('P009.h5', 'w') as f:  
        dset = f.create_dataset("euler", data = result) 
        
else:
    print("Error - Introduce los argumentos correctamente")
    print('Ejemplo: python method_euler.py xo y0 xn n')


