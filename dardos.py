# Simulacion de Juego de Dardos
import random
import math
import numpy as np
# Graficar
import matplotlib
import matplotlib.pyplot as plt

print("Nombre del primer Participante(c): ", end="")
jugador1 = input()
print("Nombre del segundo participante(s): ", end="")
jugador2 = input()
print("Nombre del tercer Participante(c): ", end="")
jugador3 = input()
print("Nombre del cuarta participante(s): ", end="")
jugador4 = input()

###Variabñes de interacciones
n = 100
n2 = 200
n3 = 300

##100 interacciones
# Premios, representan a cada jud¿gadir
premio = 0
premio_2 = 0
premio_3 = 0
premio_4 = 0

# int1
count_red = 0
count_yellow = 0
count_green = 0
count_no_valid = 0
count_white = 0
# intent 2
count_red_2 = 0
count_yellow_2 = 0
count_green_2 = 0
count_no_valid_2 = 0
count_white_2 = 0
# intent 3
count_red_3 = 0
count_yellow_3 = 0
count_green_3 = 0
count_no_valid_3 = 0
count_white_3 = 0
# intent 4
count_red_4 = 0
count_yellow_4 = 0
count_green_4 = 0
count_no_valid_4 = 0
count_white_4 = 0

##200 interacciones
# Premios
premio_200 = 0
premio_200_2 = 0
premio_200_3 = 0
premio_200_4 = 0

# int1
count_red_200 = 0
count_yellow_200 = 0
count_green_200 = 0
count_no_valid_200 = 0
count_white_200 = 0
# intent 2
count_red_200_2 = 0
count_yellow_200_2 = 0
count_green_200_2 = 0
count_no_valid_200_2 = 0
count_white_200_2 = 0
# intent 3
count_red_200_3 = 0
count_yellow_200_3 = 0
count_green_200_3 = 0
count_no_valid_200_3 = 0
count_white_200_3 = 0
# intent 4
count_red_200_4 = 0
count_yellow_200_4 = 0
count_green_200_4 = 0
count_no_valid_200_4 = 0
count_white_200_4 = 0
##300 interacciones
# Premios
premio_300 = 0
premio_300_2 = 0
premio_300_3 = 0
premio_300_4 = 0

# int1
count_red_300 = 0
count_yellow_300 = 0
count_green_300 = 0
count_no_valid_300 = 0
count_white_300 = 0
# intent 2
count_red_300_2 = 0
count_yellow_300_2 = 0
count_green_300_2 = 0
count_no_valid_300_2 = 0
count_white_300_2 = 0
# intent 3
count_red_300_3 = 0
count_yellow_300_3 = 0
count_green_300_3 = 0
count_no_valid_300_3 = 0
count_white_300_3 = 0
# intent 4
count_red_300_4 = 0
count_yellow_300_4 = 0
count_green_300_4 = 0
count_no_valid_300_4 = 0
count_white_300_4 = 0
# Calculo de cuatro intento en las 100 interacciones igual probabilidad por zona
for i in range(4):
    if i == 0:
        for i in range(n):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.25):  # zona roja
                premio = premio + 10
                count_red = count_red + 1

            elif (d > 0.25 and d <= 0.5):  # zona amarilla
                premio = premio + 8
                count_yellow = count_yellow + 1
            elif (d > 0.5 and d <= 0.75):  # zona verde
                premio = premio + 4
                count_green = count_green + 1
            elif (d > 0.75 and d <= 1):  # zona blanca
                premio = premio + 1
                count_white = count_white + 1

            else:
                count_no_valid = count_no_valid + 1
    if i == 1:
        print("2")
        for i in range(n):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.25):  # zona roja
                premio_2 = premio_2 + 10
                count_red_2 = count_red_2 + 1

            elif (d > 0.25 and d <= 0.5):  # zona amarilla
                premio_2 = premio_2 + 8
                count_yellow_2 = count_yellow_2 + 1
            elif (d > 0.5 and d <= 0.75):  # zona verde
                premio_2 = premio_2 + 4
                count_green_2 = count_green_2 + 1
            elif (d > 0.75 and d <= 1):  # zona blanca
                premio_2 = premio_2 + 1
                count_white_2 = count_white_2 + 1

            else:
                count_no_valid_2 = count_no_valid_2 + 1
    if i == 2:
        print("3")
        for i in range(n):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.25):  # zona roja
                premio_3 = premio_3 + 10
                count_red_3 = count_red_3 + 1

            elif (d > 0.25 and d <= 0.5):  # zona amarilla
                premio_3 = premio_3 + 8
                count_yellow_3 = count_yellow_3 + 1
            elif (d > 0.5 and d <= 0.75):  # zona verde
                premio_3 = premio_3 + 4
                count_green_3 = count_green_3 + 1
            elif (d > 0.75 and d <= 1):  # zona blanca
                premio_3 = premio_3 + 1
                count_white_3 = count_white_3 + 1

            else:
                count_no_valid_3 = count_no_valid_3 + 1
    if i == 3:
        print("4")
        for i in range(n):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.25):  # zona roja
                premio_4 = premio_4 + 10
                count_red_4 = count_red_4 + 1

            elif (d > 0.25 and d <= 0.5):  # zona amarilla
                premio_4 = premio_4 + 8
                count_yellow_4 = count_yellow_4 + 1
            elif (d > 0.5 and d <= 0.75):  # zona verde
                premio_4 = premio_4 + 4
                count_green_4 = count_green_4 + 1
            elif (d > 0.75 and d <= 1):  # zona blanca
                premio_4 = premio_4 + 1
                count_white_4 = count_white_4 + 1

            else:
                count_no_valid_4 = count_no_valid_4 + 1

# Calculo de cuatro intento en las 200 interacciones probabilida no uniforme
for i in range(4):
    if i == 0:
        for i in range(n2):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.15):  # zona roja
                premio_200 = premio_200 + 10
                count_red_200 = count_red_200 + 1

            elif (d > 0.15 and d <= 0.4):  # zona amarilla
                premio_200 = premio_200 + 8
                count_yellow_200 = count_yellow_200 + 1
            elif (d > 0.4 and d <= 0.65):  # zona verde
                premio_200 = premio_200 + 4
                count_green_200 = count_green_200 + 1
            elif (d > 0.65 and d <= 1):  # zona blanca
                premio_200 = premio_200 + 1
                count_white_200 = count_white_200 + 1

            else:
                count_no_valid_200 = count_no_valid_200 + 1
    if i == 1:
        print("2")
        for i in range(n2):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.15):  # zona roja
                premio_200_2 = premio_200_2 + 10
                count_red_200_2 = count_red_200_2 + 1

            elif (d > 0.15 and d <= 0.4):  # zona amarilla
                premio_200_2 = premio_200_2 + 8
                count_yellow_200_2 = count_yellow_200_2 + 1
            elif (d > 0.4 and d <= 0.65):  # zona verde
                premio_200_2 = premio_200_2 + 4
                count_green_200_2 = count_green_200_2 + 1
            elif (d > 0.65 and d <= 1):  # zona blanca
                premio_200_2 = premio_200_2 + 1
                count_white_200_2 = count_white_200_2 + 1

            else:
                count_no_valid_200_2 = count_no_valid_200_2 + 1
    if i == 2:
        print("3")
        for i in range(n2):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.15):  # zona roja
                premio_200_3 = premio_200_3 + 10
                count_red_200_3 = count_red_200_3 + 1

            elif (d > 0.15 and d <= 0.4):  # zona amarilla
                premio_200_3 = premio_200_3 + 8
                count_yellow_200_3 = count_yellow_200_3 + 1
            elif (d > 0.4 and d <= 0.65):  # zona verde
                premio_200_3 = premio_200_3 + 4
                count_green_200_3 = count_green_200_3 + 1
            elif (d > 0.65 and d <= 1):  # zona blanca
                premio_200_3 = premio_200_3 + 1
                count_white_200_3 = count_white_200_3 + 1

            else:
                count_no_valid_200_3 = count_no_valid_200_3 + 1
    if i == 3:
        print("4")
        for i in range(n2):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.15):  # zona roja
                premio_200_4 = premio_200_4 + 10
                count_red_200_4 = count_red_200_4 + 1

            elif (d > 0.15 and d <= 0.4):  # zona amarilla
                premio_200_4 = premio_200_4 + 8
                count_yellow_200_4 = count_yellow_200_4 + 1
            elif (d > 0.4 and d <= 0.65):  # zona verde
                premio_200_4 = premio_200_4 + 4
                count_green_200_4 = count_green_200_4 + 1
            elif (d > 0.65 and d <= 1):  # zona blanca
                premio_200_4 = premio_200_4 + 1
                count_white_200_4 = count_white_200_4 + 1

            else:
                count_no_valid_200_4 = count_no_valid_200_4 + 1

# Calculo de cuatro intento en las 300 interacciones con probabilidad no uniforme en cada zona
for i in range(4):
    if i == 0:
        for i in range(n3):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.1):  # zona roja
                premio_300 = premio_300 + 10
                count_red_300 = count_red_300 + 1

            elif (d > 0.1 and d <= 0.25):  # zona amarilla
                premio_300 = premio_300 + 8
                count_yellow_300 = count_yellow_300 + 1
            elif (d > 0.25 and d <= 0.5):  # zona verde
                premio_300 = premio_300 + 4
                count_green_300 = count_green_300 + 1
            elif (d > 0.5 and d <= 1):  # zona blanca
                premio_300 = premio_300 + 1
                count_white_300 = count_white_300 + 1

            else:
                count_no_valid_300 = count_no_valid_300 + 1
    if i == 1:
        print("2")
        for i in range(n3):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.1):  # zona roja
                premio_300_2 = premio_300_2 + 10
                count_red_300_2 = count_red_300_2 + 1

            elif (d > 0.1 and d <= 0.25):  # zona amarilla
                premio_300_2 = premio_300_2 + 8
                count_yellow_300_2 = count_yellow_300_2 + 1
            elif (d > 0.25 and d <= 0.5):  # zona verde
                premio_300_2 = premio_300_2 + 4
                count_green_300_2 = count_green_300_2 + 1
            elif (d > 0.5 and d <= 1):  # zona blanca
                premio_300_2 = premio_300_2 + 1
                count_white_300_2 = count_white_300_2 + 1

            else:
                count_no_valid_300_2 = count_no_valid_300_2 + 1
    if i == 2:
        print("3")
        for i in range(n3):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.1):  # zona roja
                premio_300_3 = premio_300_3 + 10
                count_red_300_3 = count_red_300_3 + 1

            elif (d > 0.1 and d <= 0.25):  # zona amarilla
                premio_300_3 = premio_300_3 + 8
                count_yellow_300_3 = count_yellow_300_3 + 1
            elif (d > 0.25 and d <= 0.5):  # zona verde
                premio_300_3 = premio_300_3 + 4
                count_green_300_3 = count_green_300_3 + 1
            elif (d > 0.5 and d <= 1):  # zona blanca
                premio_300_3 = premio_300_3 + 1
                count_white_300_3 = count_white_300_3 + 1

            else:
                count_no_valid_300_3 = count_no_valid_300_3 + 1
    if i == 3:
        print("4")
        for i in range(n3):
            x = 1 - 2 * (random.random())
            y = 1 - 2 * (random.random())
            d = math.sqrt(x ** 2 + y ** 2)  # distancia
            # contador de veces en cada zona
            if (d <= 0.1):  # zona roja
                premio_300_4 = premio_300_4 + 10
                count_red_300_4 = count_red_300_4 + 1

            elif (d > 0.1 and d <= 0.25):  # zona amarilla
                premio_300_4 = premio_300_4 + 8
                count_yellow_300_4 = count_yellow_300_4 + 1
            elif (d > 0.25 and d <= 0.5):  # zona verde
                premio_300_4 = premio_300_4 + 4
                count_green_300_4 = count_green_300_4 + 1
            elif (d > 0.5 and d <= 1):  # zona blanca
                premio_300_4 = premio_300_4 + 1
                count_white_300_4 = count_white_300_4 + 1

            else:
                count_no_valid_300_4 = count_no_valid_300_4 + 1

# **************************Medias*********************************************************
# Medias 100 interacciones
media_red_zone = np.array([count_red, count_red_2, count_red_3, count_red_4]).mean()
media_yellow_zone = np.array([count_yellow, count_yellow_2, count_yellow_3, count_yellow_4]).mean()
media_green_zone = np.array([count_green, count_green_2, count_green_3, count_green_4]).mean()
media_white_zone = np.array([count_white, count_white_2, count_white_3, count_white_4]).mean()

# Medias 200 interacciones
media_red_zone_200 = np.array([count_red_200, count_red_200_2, count_red_200_3, count_red_200_4]).mean()
media_yellow_zone_200 = np.array([count_yellow_200, count_yellow_200_2, count_yellow_200_3, count_yellow_200_4]).mean()
media_green_zone_200 = np.array([count_green_200, count_green_200_2, count_green_200_3, count_green_200_4]).mean()
media_white_zone_200 = np.array([count_white_200, count_white_200_2, count_white_200_3, count_white_200_4]).mean()

# Medias 300 interacciones
media_red_zone_300 = np.array([count_red_300, count_red_300_2, count_red_300_3, count_red_300_4]).mean()
media_yellow_zone_300 = np.array([count_yellow_300, count_yellow_300_2, count_yellow_300_3, count_yellow_300_4]).mean()
media_green_zone_300 = np.array([count_green_300, count_green_300_2, count_green_300_3, count_green_300_4]).mean()
media_white_zone_300 = np.array([count_white_300, count_white_300_2, count_white_300_3, count_white_300_4]).mean()

#*****************************Salidas********************
##Salida de las 100 interacciones
# SALIDA primer intento
print('********100 interacciones************')
print(' El total ganado del primer jugador es:')
print(premio)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red,count_red/n))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow,count_yellow/n))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green,count_green/n))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white,count_white/n))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid,count_no_valid/n))
#salida segundi intentp4
print(' El total ganado del segundo jugador es:')
print(premio_2)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_2,count_red_2/n))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_2,count_yellow_2/n))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_2,count_green_2/n))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_2,count_white_2/n))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_2,count_no_valid_2/n))
#salida tercer intentp4
print(' El total ganado del tercer jugador es:')
print(premio_3)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_3,count_red_3/n))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_3,count_yellow_3/n))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_3,count_green_3/n))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_3,count_white_3/n))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_3,count_no_valid_3/n))
#salida cuarto intentp4
print(' El total ganado del cuarto jugador es:')
print(premio_4)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_4,count_red_4/n))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_4,count_yellow_4/n))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_4,count_green_4/n))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_4,count_white_4/n))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_4,count_no_valid_4/n))
##Salida de las 200 interacciones
# SALIDA primer intento
print('********200 interacciones************')
print(' El total ganado del primer jugador es:')
print(premio_200)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_200,count_red_200/n2))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_200,count_yellow_200/n2))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_200,count_green_200/n2))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_200,count_white_200/n2))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_200,count_no_valid_200/n2))
#salida segundi intentp4
print(' El total ganado del segundo jugador es:')
print(premio_200_2)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_200_2,count_red_200_2/n2))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_200_2,count_yellow_200_2/n2))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_200_2,count_green_200_2/n2))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_200_2,count_white_200_2/n2))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_200_2,count_no_valid_200_2/n2))
#salida tercer intentp4
print(' El total ganado del tercer jugador es:')
print(premio_200_3)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_200_3,count_red_200_3/n2))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_200_3,count_yellow_200_3/n2))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_200_3,count_green_200_3/n2))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_200_3,count_white_200_3/n2))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_200_3,count_no_valid_200_3/n2))
#salida cuarto intentp4
print(' El total ganado del cuarto jugador es:')
print(premio_200_4)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_200_4,count_red_200_4/n2))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_200_4,count_yellow_200_4/n2))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_200_4,count_green_200_4/n2))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_200_4,count_white_200_4/n2))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_200_4,count_no_valid_200_4/n2))

##Salida de las 300 interacciones
# SALIDA primer intento
print('********300 interacciones************')
print(' El total ganado del primer jugador es:')
print(premio_300)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_300,count_red_300/n3))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_300,count_yellow_300/n3))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_300,count_green_300/n3))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_300,count_white_300/n3))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_300,count_no_valid_300/n3))
#salida segundi intentp4
print(' El total ganado del segundo jugador es:')
print(premio_300_2)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_300_2,count_red_300_2/n3))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_300_2,count_yellow_300_2/n3))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_300_2,count_green_300_2/n3))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_300_2,count_white_300_2/n3))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_300_2,count_no_valid_300_2/n3))
#salida tercer intentp4
print(' El total ganado del tercer jugador es:')
print(premio_300_3)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_300_3,count_red_300_3/n3))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_300_3,count_yellow_300_3/n3))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_300_3,count_green_300_3/n3))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_300_3,count_white_300_3/n3))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_300_3,count_no_valid_300_3/n3))
#salida cuarto intentp4
print(' El total ganado del cuarto jugador es:')
print(premio_300_4)
print('Numero de Dardos en Zona Roja es: %s,Prob: %s'%(count_red_300_4,count_red_300_4/n3))
print('Numero de Dardos en Zona Amarilla es: %s,Prob: %s'%(count_yellow_300_4,count_yellow_300_4/n3))
print('Numero de Dardos en Zona Verde es: %s,Prob: %s'%(count_green_300_4,count_green_300_4/n3))
print('Numero de Dardos en Zona Blanca es: %s,Prob: %s'%(count_white_300_4,count_white_300_4/n3))
print('Numero de Dardos que no caen en una zona valida: %s,Prob: %s'%(count_no_valid_300_4,count_no_valid_300_4/n3))
#medias
#//100 interacciones
print("***************Medias de las 100 interacciones*************")
print('Numero Medio de Dardos en Zona Roja es: %s'%(media_red_zone))
print('Numero de Dardos en Zona Amarilla es: %s'%(media_yellow_zone))
print('Numero de Dardos en Zona Verde es: %s'%(media_green_zone))
print('Numero de Dardos en Zona Blanca es: %s'%(media_white_zone))
#200 interacciones
print("***************Medias de las 200 interacciones*************")
print('Numero Medio de Dardos en Zona Roja es: %s'%(media_red_zone_200))
print('Numero de Dardos en Zona Amarilla es: %s'%(media_yellow_zone_200))
print('Numero de Dardos en Zona Verde es: %s'%(media_green_zone_200))
print('Numero de Dardos en Zona Blanca es: %s'%(media_white_zone_200))
#300 interacciones
print("***************Medias de las 300 interacciones*************")
print('Numero Medio de Dardos en Zona Roja es: %s'%(media_red_zone_300))
print('Numero de Dardos en Zona Amarilla es: %s'%(media_yellow_zone_300))
print('Numero de Dardos en Zona Verde es: %s'%(media_green_zone_300))
print('Numero de Dardos en Zona Blanca es: %s'%(media_white_zone_300))


# ************Ganadopres*************
ganador_100 = 0
ganador_200 = 0
ganador_300 = 0
# Ganador en las 100 interacciones
print("*****Resultados de las 100 interacciones*******")
if max(premio, premio_2, premio_3, premio_4) == premio:
    ganador_100 = premio
    print("gano ", jugador1, "con", ganador_100, " puntos")
elif max(premio, premio_2, premio_3, premio_4) == premio_2:
    ganador_100 = premio
    print("gano ", jugador2, "con", ganador_100, " puntos")
elif max(premio, premio_2, premio_3, premio_4) == premio_3:
    ganador_100 = premio
    print("gano ", jugador3, "con", ganador_100, " puntos")
elif max(premio, premio_2, premio_3, premio_4) == premio_4:
    ganador_100 = premio
    print("gano ", jugador4, "con", ganador_100, " puntos")
# Ganador en las 200 interacciones
print("*****Resultados de las 200 interacciones*******")
if max(premio_200, premio_200_2, premio_200_3, premio_200_4) == premio_200:
    ganador_200 = premio_200
    print("gano ", jugador1, "con", ganador_200, " puntos")
elif max(premio_200, premio_200_2, premio_200_3, premio_200_4) == premio_200_2:
    ganador_200 = premio_200_2
    print("gano ", jugador2, "con", ganador_200, " puntos")
elif max(premio_200, premio_200_2, premio_200_3, premio_200_4) == premio_200_3:
    ganador_200 = premio_200_3
    print("gano ", jugador3, "con", ganador_200, " puntos")
elif max(premio_200, premio_200_2, premio_200_3, premio_200_4) == premio_200_4:
    ganador_200 = premio_200_4
    print("gano ", jugador4, "con", ganador_200, " puntos")
# Ganador en las 300 interacciones
print("*****Resultados de las 300 interacciones*******")
if max(premio_300, premio_300_2, premio_300_3, premio_300_4) == premio_300:
    ganador_300 = premio_300
    print("gano ", jugador1, "con", ganador_300, " puntos")
elif max(premio_300, premio_300_2, premio_300_3, premio_300_4) == premio_300_2:
    ganador_300 = premio_300_2
    print("gano ", jugador2, "con", ganador_300, " puntos")
elif max(premio_300, premio_300_2, premio_300_3, premio_300_4) == premio_300_3:
    ganador_300 = premio_300_3
    print("gano ", jugador3, "con", ganador_300, " puntos")
elif max(premio_300, premio_300_2, premio_300_3, premio_300_4) == premio_300_4:
    ganador_300 = premio_300_4
    print("gano ", jugador4, "con", ganador_300, " puntos")

# ************************Graficas*****************************************************************
# Graficas
#*****100 interacciones***
#primer jugador
##########################################################33
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red,count_yellow, count_green, count_white, count_no_valid]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Primer jugador en las 100 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Primer jugador en las 100 interacciones")
    
    plt.ylabel('veces en zona')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#segundo jugadoe
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_2,count_yellow_2, count_green_2, count_white_2, count_no_valid_2]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Segundo jugador en las 100 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Segundo jugador en las 100 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#tercer jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_3,count_yellow_3, count_green_3, count_white_3, count_no_valid_3]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Tercer jugador en las 100 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Tercer jugador en las 100 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#cuarto jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_4,count_yellow_4, count_green_4, count_white_4, count_no_valid_4]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Cuarto jugador en las 100 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Cuarto jugador en las 100 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#*********************200 interacciones*************
#primer jugador
##########################################################33
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_200,count_yellow_200, count_green_200, count_white_200, count_no_valid_200]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Primer jugador en las 200 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Primer jugador en las 200 interacciones")
    plt.ylabel('veces en zona')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#segundo jugadoe
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_200_2,count_yellow_200_2, count_green_200_2, count_white_200_2, count_no_valid_200_2]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Segundo jugador en las 200 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Segundo jugador en las 200 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#tercer jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_200_3,count_yellow_200_3, count_green_200_3, count_white_200_3, count_no_valid_200_3]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Tercer jugador en las 200 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Tercer jugador en las 200 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#cuarto jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_200_4,count_yellow_200_4, count_green_200_4, count_white_200_4, count_no_valid_200_4]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Cuarto jugador en las 200 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Cuarto jugador en las 200 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#****************300 interacciones****************
#primer jugador
##########################################################33
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido","s"]
means = [count_red_300,count_yellow_300, count_green_300, count_white_300, count_no_valid_300]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Primer jugador en las 300 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Primer jugador en las 300 interacciones")
    plt.ylabel('veces en zona')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#segundo jugadoe
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_300_2,count_yellow_300_2, count_green_300_2, count_white_300_2, count_no_valid_300_2]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Segundo jugador en las 300 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Segundo jugador en las 300 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#tercer jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_300_3,count_yellow_300_3, count_green_300_3, count_white_300_3, count_no_valid_300_3]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Tercer jugador en las 300 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Tercer jugador en las 300 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#cuarto jugador
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco", "no valido"]
means = [count_red_300_4,count_yellow_300_4, count_green_300_4, count_white_300_4, count_no_valid_300_4]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Cuarto jugador en las 300 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Cuarto jugador en las 300 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#*******medias***
#100 interaciones
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco"]
means = [ media_red_zone, media_yellow_zone, media_green_zone, media_white_zone]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Media en las 100 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Media en las 100 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#200 interaciones
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco"]
means = [ media_red_zone_200, media_yellow_zone_200, media_green_zone_200, media_white_zone_200]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Media en las 200 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Media en las 200 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()
#300 interaciones
colors = ["r", "y", "g", "#948F94","b"]
models = ["rojo","amarillo", "verde", "blanco"]
means = [ media_red_zone_300, media_yellow_zone_300, media_green_zone_300, media_white_zone_300]
cvs =   [count_yellow, count_green, count_red, count_white, count_no_valid]
cont = 0
for m, c, mean in zip(models, colors, means):
    plt.figure("Media en las 300 interacciones")
    plt.bar(cont, mean, color = c)
    plt.text(cont-0.16, mean + 0.03, "{:.2f}".format(mean))
    plt.title("Media en las 300 interacciones")
    plt.ylabel('veces en zonas')
    plt.xticks(np.arange(len(models)), models)
    cont+=1

plt.legend()
plt.tight_layout()
plt.show()